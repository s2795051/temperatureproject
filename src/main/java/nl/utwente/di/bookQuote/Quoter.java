package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    public double getBookPrice(String celsius) {
        return (Double.parseDouble(celsius) * 9 / 5 + 32);
    }
}
